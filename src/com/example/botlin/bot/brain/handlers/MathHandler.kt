package com.example.botlin.bot.brain.handlers

import com.example.botlin.bot.beans.Message

class MathHandler() : MessageHandler {
    private fun buildRegex(): Regex {
        return Regex("(\\d+)\\s*\\+\\s*(\\d+)")
    }

    override fun acceptMessage(msg: Message): Boolean {
        val regex = this.buildRegex()
        return regex.matches(msg.toString().trim())
    }

    override fun handleMessage(msg: Message): String {
        val match = this.buildRegex().matchEntire(msg.toString())
        if (match != null) {
            val match1 = match.groups[1]?.value?.toInt()
            val match2 = match.groups[2]?.value?.toInt()
            if (match1 != null && match2 != null) {
                val result = (match1 + match2).toString()
                return "The result of $msg is $result"
            }
        }
        throw IllegalArgumentException("\"$msg\" is not a valid argument for this handler")
    }

}
