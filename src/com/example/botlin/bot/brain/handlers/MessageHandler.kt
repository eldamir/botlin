package com.example.botlin.bot.brain.handlers

import com.example.botlin.bot.beans.Message

interface MessageHandler {
    fun acceptMessage(msg: Message): Boolean
    fun handleMessage(msg: Message): String
}