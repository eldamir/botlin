package com.example.botlin.bot.beans


data class Message(private var content: String) {
    override fun toString(): String {
        return this.content.toString()
    }
}