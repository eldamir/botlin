package com.example.botlin.bot

import com.example.botlin.bot.beans.Message
import com.example.botlin.bot.brain.handlers.MessageHandler
import java.util.*

class Bot {
    val handlers: ArrayList<MessageHandler> = ArrayList()

    fun addHandler(handler: MessageHandler) {
        this.handlers.add(handler)
    }

    fun handleMessage(message: Message): String {
        for (handler in this.handlers) {
            if (handler.acceptMessage(message)) {
                return handler.handleMessage(message)
            }
        }
        return "I received \"$message\", and don't know what to do with it"
    }
}

