package com.example.botlin

import com.example.botlin.bot.Bot
import com.example.botlin.bot.beans.Message
import com.example.botlin.bot.brain.handlers.MathHandler


fun main(args : Array<String>) {
    // Create a bot instance
    val bot = Bot()
    bot.addHandler(MathHandler())

    println(bot.handleMessage(Message("Hello, world!")))
    println(bot.handleMessage(Message("2 + 2")))
}