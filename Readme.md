# Introduction

This project defines a Bot with no clear purpose in itself.

The point of the project is simply to get experience with Kotlin and the surrounding toolchain.

The project is completely open source, and collaboration is very welcome, be it

* Code contributions - Features, improvements, etc.
* Issues - Suggestions, Challenges, Domain practice that could be applied

# So what can it do?

*At this point, it does nothing, but the list below are some short- and long-term goals*

* The master branch is continually built and deployed by Travis CI
* A public instance of the Bot is running at ~~(http://nowhere-at-the-moment.com/sorry)~~
* Bot knows commands and responds to them
* Bot will implement some kind of machine learning so that it can determine what operation to perform, when the command is not strictly worded, so that natural language can be applied: "/time", "Show me time!", "What time is it?", "Convert Berlin time to my current timezone, please", etc.
* The Bot will accept JSON HTTP requests with data to interpret and will respond in like fashion.
* Eventually a client will be added to ease communication with the bot. A CLI with simple commands or even a GUI for the lols.